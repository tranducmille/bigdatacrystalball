package com.ductran.spark;

import java.io.Serializable;
import java.util.Comparator;

import scala.Tuple1;

public class TupleComparator implements Comparator<Tuple1<String>>,Serializable{

	private static final long serialVersionUID = 1L;

	@Override
	public int compare(Tuple1<String> o1, Tuple1<String> o2) {
		return o1.toString().compareTo(o2.toString());
	}

}
