package com.ductran.spark;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.api.java.function.PairFunction;

import scala.Tuple1;
import scala.Tuple2;

public class SparkWordCount {
	public static final Pattern apacheLogRegex = Pattern.compile("^([\\w+.][\\S+]+)");


	public static Tuple1<String> extractKey(String line) {
		Matcher m = apacheLogRegex.matcher(line);
		if (m.find()) {
			String ip = m.group(1);
			return new Tuple1<>(ip);
		}
		return new Tuple1<>(null);
	}

	public static Count extractStats(String line) {
		Matcher m = apacheLogRegex.matcher(line);
		if (m.find()) {
			return new Count(1);
		} else {
			return new Count(1);
		}
	}

	public static void main(String[] args) {
		JavaSparkContext sc = new JavaSparkContext(new SparkConf().setAppName("Spark Count"));
		try{

			JavaRDD<String> dataSet =  sc.textFile(args[0]) ;

			JavaPairRDD<Tuple1<String>, Count> extracted = dataSet
					.mapToPair(new PairFunction<String, Tuple1<String>, Count>() {
						private static final long serialVersionUID = 1L;

						@Override
						public Tuple2<Tuple1<String>, Count> call(String s) {
							return new Tuple2<>(extractKey(s), extractStats(s));
						}
					});

			JavaPairRDD<Tuple1<String>, Count> counts = extracted
					.reduceByKey(new Function2<Count, Count, Count>() {
						private static final long serialVersionUID = 1L;

						@Override
						public Count call(Count c, Count c1) {
							return c.merge(c1);
						}
					}).sortByKey(new TupleComparator(), true);

			List<Tuple2<Tuple1<String>, Count>> output = counts	.collect();
			for (Tuple2<?, ?> t : output) {
				System.out.println(t._1() + "\t" + t._2());
			}
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			sc.stop();
			sc.close();
		}
	}
}