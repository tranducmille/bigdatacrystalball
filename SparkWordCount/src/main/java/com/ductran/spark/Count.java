package com.ductran.spark;

import java.io.Serializable;


public class Count implements Serializable {

	
	private static final long serialVersionUID = 1L;
	private final int count;

	public Count(int count) {
		this.count = count;
	}

	public Count merge(Count other) {
		return new Count(count + other.count);
	}

	public String toString() {
		return String.format("Access Counter=%s", count);
	}
}