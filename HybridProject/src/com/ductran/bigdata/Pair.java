package com.ductran.bigdata;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.WritableComparable;

public class Pair implements WritableComparable<Pair> {

	private Text key;
	private Text value;		
	
	@Override
	public void readFields(DataInput in) throws IOException {
		// TODO Auto-generated method stub
		this.key.readFields(in);
		this.value.readFields(in);
	}

	@Override
	public void write(DataOutput out) throws IOException {
		// TODO Auto-generated method stub
		this.key.write(out);
		this.value.write(out);
	}

	@Override
	public int compareTo(Pair p) {
		if(p == null || !( p instanceof Pair))
			return -1;
		int cmp = key.compareTo(p.key);
		if(cmp != 0)
			return cmp;
		return value.compareTo(p.value);
	}
	
	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((key == null) ? 0 : key.hashCode());
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Pair other = (Pair) obj;
		if (key == null) {
			if (other.key != null)
				return false;
		} else if (!key.equals(other.key))
			return false;
		if (value == null) {
			if (other.value != null)
				return false;
		} else if (!value.equals(other.value))
			return false;
		return true;
	}

	public Pair(){
		this.key = new Text();
		this.value = new Text();
	}
	
	public Pair(Text i, Text j){
		this.key = i;
		this.value = j;
	}

	public Text getKey() {
		return key;
	}

	public Text getValue() {
		return value;
	}
	

}
