package com.ductran.bigdata;

import java.io.IOException;
import java.util.Iterator;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.mapreduce.Reducer;

public class HybridReducer extends Reducer<Pair, IntWritable, Text, Stripe<Writable, Writable>> {

	private Stripe<Writable, Writable> stripe = new Stripe<Writable, Writable>();

	@Override
	@SuppressWarnings("unchecked")
	public void reduce(Pair pair, Iterable<IntWritable> values, Context context)
			throws IOException, InterruptedException {

		Text w = new Text(pair.getKey());
		Text u = new Text(pair.getValue());		
		
		int sum = 0;		
		Iterator<IntWritable> itor = values.iterator();
		while(itor.hasNext()){
			sum += itor.next().get();
		}
				
		Stripe<Writable, Writable> stripeH = (Stripe<Writable, Writable>) stripe.get(w);
		if (stripeH == null) {
			stripeH = new Stripe<Writable, Writable>();
			stripeH.put(u, new Text(String.valueOf(sum)));
		}
		else {
			IntWritable tempValue = (IntWritable) stripeH.get(u);
			if (tempValue == null) {
				stripeH.put(u,new Text(String.valueOf(sum)));
			}
			else {
				tempValue.set(tempValue.get() + sum);
			}
		}
		stripe.put(w, stripeH);
		
	}
	
	@Override
	@SuppressWarnings({"unchecked","rawtypes"})	
	protected void cleanup(Context context)	throws IOException, InterruptedException {
		Stripe<Writable, Writable> stripeH;
		
		Iterator<WritableComparable> iteratorHf;
		Iterator<WritableComparable> iteratorH;
		int marginal;
		
		iteratorHf = stripe.keySet().iterator();
		while (iteratorHf.hasNext()) {
			Text w = (Text) iteratorHf.next();
			
			marginal = 0;
			stripeH = (Stripe<Writable, Writable>) stripe.get(w);
			iteratorH = stripeH.keySet().iterator();
			while (iteratorH.hasNext()) {
				Text u = (Text) iteratorH.next();				
				marginal +=  Integer.parseInt(stripeH.get(u).toString());
			}
			
			stripeH = (Stripe<Writable, Writable>) stripe.get(w);
			iteratorH = stripeH.keySet().iterator();
			while (iteratorH.hasNext()) {
				Text u = (Text) iteratorH.next();
				Text tmpVal = (Text)stripeH.get(u);
				tmpVal.set(tmpVal + "/" + marginal);
			}
			context.write(w, stripeH);
		}
	}
}
