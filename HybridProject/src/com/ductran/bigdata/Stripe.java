package com.ductran.bigdata;

import java.util.Iterator;
import java.util.Set;

import org.apache.hadoop.io.SortedMapWritable;
import org.apache.hadoop.io.WritableComparable;

public class Stripe<Writable, IntWritable> extends SortedMapWritable {

	@Override
	@SuppressWarnings("rawtypes")
	public String toString() {
		
		Set<java.util.Map.Entry<WritableComparable, org.apache.hadoop.io.Writable>> set = entrySet();
		Iterator<java.util.Map.Entry<WritableComparable, org.apache.hadoop.io.Writable>> it = set.iterator();
		StringBuilder builder = new StringBuilder();
		builder.append("{");
		int idx = 0;
		while (it.hasNext()) {
			java.util.Map.Entry<WritableComparable, org.apache.hadoop.io.Writable> wt = it.next();
			builder.append("(").append(wt.getKey() + "," + wt.getValue()).append(")");
			if (set.size() > idx + 1) {
				builder.append(",");
			}
			idx++;
		}

		builder.append("}");
		return builder.toString();
	}

}
