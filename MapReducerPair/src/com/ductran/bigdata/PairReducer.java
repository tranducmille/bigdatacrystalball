package com.ductran.bigdata;

import java.io.IOException;
import java.util.Iterator;

import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

public class PairReducer extends Reducer<Pair, IntWritable, Text, FloatWritable> {

	private int marginal = 0;	

	@Override
	public void reduce(Pair pair, Iterable<IntWritable> list, Context context)
			throws IOException, InterruptedException {

		Iterator<IntWritable> values = list.iterator();
		Text wildCard = pair.getValue();
		Text star = new Text("*");
		int sum = 0;
		
		while (values.hasNext()) {
			sum += values.next().get();			
		}
		
		if(!star.equals(wildCard)){				
			context.write(new Text("("+pair.getKey() + "," + pair.getValue() + ")    "+ sum + "/" + marginal), new FloatWritable((float) sum / marginal));
		}else{
			marginal = sum;
		}
	}
	
}
