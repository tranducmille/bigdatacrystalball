package com.ductran.bigdata;

import java.io.IOException;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

public class PairMapper extends Mapper<LongWritable, Text, Pair, IntWritable> {
	
	private final static IntWritable one = new IntWritable(1);

	@Override
	public void map(LongWritable key, Text value, Context context)
			throws IOException, InterruptedException {
		
		if(value == null || value.getLength() == 0)
			return;
		
		String[] words = value.toString().split(" ");
		int len = words.length;
		for (int i=0; i < len; i++) {
			if (words[i] != null && !words[i].isEmpty()) {
				for (int j = i + 1; j < len; j++) {
					if (words[j] != null && !words[j].isEmpty()) {
						if (words[i].equals(words[j])) {
							break;
						}
						context.write(new Pair(new Text(words[i]),new Text(words[j])), one);
						context.write(new Pair(new Text(words[i]), new Text("*")), one);
					}
                }
            }
        }
	}
	
	
}
