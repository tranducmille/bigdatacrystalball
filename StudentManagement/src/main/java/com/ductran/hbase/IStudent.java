
package com.ductran.hbase;

public interface IStudent {
	
	public static String STUDENT_TBL = "Student";
	
	public interface Columns{
		public static String ID_COLUMNS = "Id";
		public static String Name_COLUMNS = "Name";
		
	}
}
