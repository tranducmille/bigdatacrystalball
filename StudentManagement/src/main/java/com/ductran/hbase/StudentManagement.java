package com.ductran.hbase;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Admin;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;
import org.apache.hadoop.hbase.client.Delete;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.ResultScanner;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.client.Table;
import org.apache.hadoop.hbase.io.compress.Compression.Algorithm;
import org.apache.hadoop.hbase.util.Bytes;

public class StudentManagement {

	public static void createOrOverwrite(Admin admin, HTableDescriptor table)
			throws IOException {
		if (admin.tableExists(table.getTableName())) {
			admin.disableTable(table.getTableName());
			admin.deleteTable(table.getTableName());
		}
		admin.createTable(table);
	}

	public static void createSchemaTables(Configuration config)
			throws IOException {
		try {
			try (Connection connection = ConnectionFactory
					.createConnection(config);
					Admin admin = connection.getAdmin()) {

				HTableDescriptor table = new HTableDescriptor(TableName.valueOf(IStudent.STUDENT_TBL));
				table.addFamily(new HColumnDescriptor(IStudent.Columns.ID_COLUMNS).setCompressionType(Algorithm.SNAPPY));
				table.addFamily(new HColumnDescriptor(IStudent.Columns.Name_COLUMNS).setCompressionType(Algorithm.SNAPPY));

				System.out.print("Creating table Student successfully. ");
				createOrOverwrite(admin, table);
				System.out.println(" Done.");
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static boolean updateData(String rowKey, String family, String qualifier, String value, Configuration config) throws IOException {
		try (Connection connection = ConnectionFactory.createConnection(config);
				Admin admin = connection.getAdmin()) {
	      
		  Table hTable = connection.getTable(TableName.valueOf(IStudent.STUDENT_TBL));
	      Put p = new Put(Bytes.toBytes(rowKey));

	      p.addColumn(Bytes.toBytes(family),Bytes.toBytes(qualifier),Bytes.toBytes(value));

	      hTable.put(p);
	      System.out.println("Data Updated");

	      hTable.close();
		}
		return true;
	}
	
	public static boolean deleteData(String rowKey,Configuration config) throws IOException {
		try (Connection connection = ConnectionFactory.createConnection(config);
				Admin admin = connection.getAdmin()) {
	      
			Table table = connection.getTable(TableName.valueOf(IStudent.STUDENT_TBL));
	        List<Delete> list = new ArrayList<Delete>();
	        Delete del = new Delete(rowKey.getBytes());
	        list.add(del);
	        table.delete(list);
	        System.out.println("del recored row1 ok.");
		}
		return true;
	}
	
	public static void addRecord(String rowKey, String family, String qualifier, String value, Configuration config) throws Exception {
		try (Connection connection = ConnectionFactory.createConnection(config);
				Admin admin = connection.getAdmin()) {

			Table hTable = connection.getTable(TableName.valueOf(IStudent.STUDENT_TBL));
			if (hTable == null) {
				System.out.println("Table does not exist.");
				System.exit(-1);
			}
			Put p = new Put(Bytes.toBytes(rowKey));
			p.addColumn(Bytes.toBytes(family),Bytes.toBytes(qualifier), Bytes.toBytes(value));
			hTable.put(p);
			System.out.println("insert records to table " + IStudent.STUDENT_TBL + " ok.");
		}

	}

	public static void populateData(Configuration config) throws IOException {

		try (Connection connection = ConnectionFactory.createConnection(config);
				Admin admin = connection.getAdmin()) {

			Table hTable = connection.getTable(TableName.valueOf(IStudent.STUDENT_TBL));
			if (hTable == null) {
				System.out.println("Table does not exist.");
				System.exit(-1);
			}

			
			Scan s = new Scan();
			s.addColumn(Bytes.toBytes(IStudent.Columns.ID_COLUMNS), Bytes.toBytes("col1"));
			s.addColumn(Bytes.toBytes(IStudent.Columns.Name_COLUMNS), Bytes.toBytes("col2"));
			ResultScanner scanner = hTable.getScanner(s);

			try {
				for (Result rnext = scanner.next(); rnext != null; rnext = scanner.next()) {
					System.out.println("Found row : " + rnext);
				}
			} finally {
				scanner.close();
			}
		}

	}
	

	public static void main(String... args) throws Exception {
		Configuration config = HBaseConfiguration.create();

		// Add any necessary configuration files (hbase-site.xml, core-site.xml)
		config.addResource(new Path("/etc/hbase/conf", "hbase-site.xml"));
		config.addResource(new Path("/etc/hadoop/conf", "core-site.xml"));
		//createSchemaTables(config);
		//addRecord("row1", IStudent.Columns.ID_COLUMNS, "col1", "001", config);
        //addRecord("row1", IStudent.Columns.Name_COLUMNS, "col2", "Duc Tran", config);
        //addRecord("row2", IStudent.Columns.ID_COLUMNS, "col1", "002", config);
       // addRecord("row2", IStudent.Columns.Name_COLUMNS, "col2", "Johnny Tran", config);
		//populateData(config);
		//updateData("row2", IStudent.Columns.Name_COLUMNS, "col2","Andy Tran", config);
		//populateData(config);
		deleteData("row2", config);
		populateData(config);
	}

}
