## Remove all class files before packaging
rm -f bin/com/ductran/bigdata/*.class

## Compile all the file and put output folder in bin
javac -classpath `hadoop classpath` -d bin src/com/ductran/bigdata/Stripe.java src/com/ductran/bigdata/StripePartitioner.java src/com/ductran/bigdata/Main.java  src/com/ductran/bigdata/StripeMapper.java src/com/ductran/bigdata/StripeReducer.java 

## Packaging the jar file
jar -cvfe main.jar com.ductran.bigdata.Main -C bin .

## Make the input folder
hadoop fs -mkdir -p /user/cloudera/input

## Put the test file in input folder in hdsf		
hadoop fs -put /home/cloudera/bigdata/input/input.txt /user/cloudera/input

## Make sure output folder remove before run 
hadoop fs -rmr output

## Execute the Pair project
hadoop jar main.jar /user/cloudera/input /user/cloudera/output

## Print the output file in command line
hadoop fs -get /user/cloudera/output/part-r-00000 Output

cat Output

