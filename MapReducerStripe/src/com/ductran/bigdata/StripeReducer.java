package com.ductran.bigdata;

import java.io.IOException;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.mapreduce.Reducer;

public class StripeReducer extends Reducer<Text, Stripe<Writable, IntWritable>, Text, Stripe<Writable, IntWritable>> {

	private Stripe<Writable, IntWritable> map = new Stripe<>();
	
	@Override
	public void reduce(Text term, Iterable<Stripe<Writable, IntWritable>> list, Context context)	throws IOException, InterruptedException {
		
		Iterator<Stripe<Writable, IntWritable>> values = list.iterator();
		map.clear();
		int marginal = 0;
		// sum up all stripe
		while(values.hasNext()){
			Stripe<Writable, IntWritable> value = (Stripe<Writable, IntWritable>)values.next(); 
			Set<Writable> keys = value.keySet();
			for (Writable key : keys) {
				IntWritable fromCount = (IntWritable) value.get(key);
				marginal += fromCount.get();
				if (map.containsKey(key)) {
					Text count = (Text) map.get(key);
					count.set(new Text(String.valueOf(Integer.valueOf(count.toString()) + fromCount.get())));

				} else {
					map.put(key, new Text(String.valueOf(fromCount.get())));
				}
			}			
		}
		
		
		for(Entry<Writable, Writable> entry : map.entrySet()){
			if(!entry.getKey().equals(term)){			
				Text val = (Text)map.get(entry.getKey());
				val.set(val + "/"+ marginal);
			}
			
		}
		context.write(term, map);
	}
}
