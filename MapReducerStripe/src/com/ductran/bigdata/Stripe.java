package com.ductran.bigdata;

import java.util.Iterator;
import java.util.Set;

import org.apache.hadoop.io.MapWritable;

public class Stripe<Writable, IntWritable> extends MapWritable {

	@Override
	public String toString() {
		Set<java.util.Map.Entry<org.apache.hadoop.io.Writable, org.apache.hadoop.io.Writable>> set = entrySet();
		Iterator<java.util.Map.Entry<org.apache.hadoop.io.Writable, org.apache.hadoop.io.Writable>> it = set.iterator();
		StringBuilder builder = new StringBuilder();
		builder.append("{");
		int idx = 0;
		while (it.hasNext()) {
			Entry<org.apache.hadoop.io.Writable, org.apache.hadoop.io.Writable> wt = it.next();
			builder.append("(").append(wt.getKey() + "," + wt.getValue()).append(")");
			if (set.size() > idx + 1) {
				builder.append(",");
			}
			idx++;
		}

		builder.append("}");
		return builder.toString();
	}

}
