package com.ductran.bigdata;

import java.io.IOException;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.mapreduce.Mapper;

public class StripeMapper extends
		Mapper<LongWritable, Text, Writable, Stripe<Writable, IntWritable>> {

	private IntWritable one = new IntWritable(1);	

	@Override
	public void map(LongWritable key, Text value, Context context)
			throws IOException, InterruptedException {

		if (value == null || value.getLength() == 0)
			return;

		String[] words = value.toString().split(" ");
		Stripe<Writable, IntWritable> stripe ;
		
		int len = words.length;
		
		for (int i = 0; i < len ; i++) {
			if (words[i] != null && !words[i].isEmpty()) {
				stripe = new Stripe<>();
				for (int j = i + 1; j < len; j++) {
					if (words[j] != null && !words[j].isEmpty()) {
						if (words[i].equals(words[j])) {
							break;
						}
						Text term = new Text(words[j]);
						IntWritable val = (IntWritable) stripe.get(term);

						if (val == null) {
							val = one;
						} else {
							val = new IntWritable(val.get() + 1);
						}

						stripe.put(term, val);
					}
				}
				context.write(new Text(words[i]), stripe);
			}
		}

	}

}